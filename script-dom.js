let secao = document.querySelector(".section_mensagem");

function enviar_mensagem() {
        let input = document.querySelector(".input_mensagem");
        let escopo = document.createElement("div");
        let escopobotao = document.createElement("div");
        let texto = document.createElement("p");
        let botaolimpar = document.createElement("button");
        let botaoeditar = document.createElement("button");
        texto.innerText = input.value;
        escopo.classList.add("mensagem_nova")
        botaoeditar.classList.add("editar")
        botaoeditar.append("Editar")
        botaolimpar.append("Limpar")
        botaolimpar.id="btn_limpar"
        escopo.append(texto);
        escopobotao.append(botaoeditar)
        escopobotao.append(botaolimpar)
        escopo.append(escopobotao);
        escopobotao.classList.add("escopobotao")
        secao.append(escopo);
        botaolimpar.addEventListener("click", (e)=>{
            let i = document.querySelector(".mensagem_nova")
            i.remove()
            console.log(e.target.parentNode)})       
}


let btn_enviar = document.querySelector("#btn_enviar");
btn_enviar.addEventListener("click", ()=>{enviar_mensagem()})

console.log(secao.innerHTML)